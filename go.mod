module gitlab.com/silecs/php-slowlog

go 1.15

require (
	github.com/lmorg/apachelogs v0.0.0-20161115121556-e5f3eae677ad
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	modernc.org/sqlite v1.7.4
)
