package cmd

import (
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/silecs/php-slowlog/internal/sqlite"
)

var analyzeCmd = &cobra.Command{
	Use:   "analyze [blocking-function-name]",
	Short: "Matches the selected slow log with the access log.",
	Long: `Matches the selected slow log with the access log.
This will fill in the "link" table.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		wwwRoot := strings.TrimRight(viper.GetString("wwwroot"), "/")
		duration := viper.GetInt("duration")
		crossLogs(args[0], wwwRoot, duration)
	},
}

func init() {
	rootCmd.AddCommand(analyzeCmd)
	analyzeCmd.Flags().String("wwwroot", "", "path to the www root for the php slow log")
	analyzeCmd.Flags().Int("duration", 10, "number of seconds that activates the slow log")
	viper.BindPFlags(analyzeCmd.Flags())
}

func crossLogs(funcName string, wwwRoot string, seconds int) {
	context := sqlite.InitDB("/tmp/php-slowlog.sqlite")
	defer context.Close()

	context.ResetAnalysis()
	from, to := context.GetAccessInterval()
	events := context.FindEvents(funcName, from, to, 50)
	fmt.Printf("Found %d slow events in this interval.\n", len(events))
	for _, e := range events {
		uri := strings.TrimPrefix(e.Path, wwwRoot)
		context.FindAccess(&e, uri, seconds, funcName)
	}
	context.PrintHistories(funcName)
}
