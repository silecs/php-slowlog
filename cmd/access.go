package cmd

import (
	"log"

	"github.com/lmorg/apachelogs"
	"github.com/spf13/cobra"
	"gitlab.com/silecs/php-slowlog/internal/sqlite"
)

var accessCmd = &cobra.Command{
	Use:   "access [/path/to/access.log*]",
	Short: "parse the access logs of Apache httpd.",
	Long: `Parse the access logs of Apache httpd into a sqlite DB.

Gzipped files are accepted.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		parseApacheAccessLogs(args)
	},
}

func init() {
	rootCmd.AddCommand(accessCmd)
}

func parseApacheAccessLogs(files []string) {
	context := sqlite.InitDB("/tmp/php-slowlog.sqlite")
	defer context.Close()

	context.ResetAccess()
	context.StartAccessBatch()
	errHandler := func(err error) {
		log.Fatalln(err)
	}
	for _, filename := range files {
		apachelogs.ReadAccessLog(filename, context.WriteAccess, errHandler)
	}
	context.EndAccessBatch()
}
