package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "php-slowlog",
	Short: "Parse the slowlog of php-fpm",
	Long: `Parse the slowlog of php-fpm into a sqlite DB.
It can also parse the Apache access log and cross-match the logs.

Typical usage:
php-slowlog phpslow /var/log/php*-fpm-slow.log*
php-slowlog access /var/log/apache2/access.log*
php-slowlog analyze --wwwroot=/srv/www --duration=60 name_of_blocking_function
`,
}

// Execute will run the sub command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// flags shared by all the commands
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.php-slowlog.toml)")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".php-slowlog" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".php-slowlog")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
