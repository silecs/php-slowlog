package cmd

import (
	"compress/gzip"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/silecs/php-slowlog/internal/parser"
	"gitlab.com/silecs/php-slowlog/internal/sqlite"
)

var phpslowCmd = &cobra.Command{
	Use:   "phpslow [logfiles]",
	Short: "parse a php-fpm log of slow requests.",
	Long: `Parse a php-fpm log of slow requests.

The log must be enabled through the php-fpm pool config.
Gzipped filed are accepted.`,
	Args: cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		parsePhpSlowlog(args)
	},
}

func init() {
	rootCmd.AddCommand(phpslowCmd)
	// phpslowCmd.PersistentFlags().String("foo", "", "A help for foo")
}

func parsePhpSlowlog(files []string) {
	context := sqlite.InitDB("/tmp/php-slowlog.sqlite")
	defer context.Close()

	context.ResetEvents()
	for _, filename := range files {
		readEvents(filename, context)
	}
	context.PrintEventsSummary()
}

func readEvents(filename string, into sqlite.Context) {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	var fparser *parser.Parser
	if strings.HasSuffix(filename, ".gz") {
		gzreader, err := gzip.NewReader(file)
		if err != nil {
			panic(err)
		}
		defer gzreader.Close()
		fparser = parser.NewParser(gzreader, filename)
	} else {
		fparser = parser.NewParser(file, filename)
	}

	for {
		e := fparser.NextEvent()
		if e == nil {
			break
		}
		into.WriteEvent(e)
	}
}
