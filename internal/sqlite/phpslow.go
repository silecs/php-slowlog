package sqlite

import (
	"encoding/csv"
	"os"
	"strings"

	"gitlab.com/silecs/php-slowlog/internal/parser"
)

func (c Context) ResetEvents() {
	c.db.Exec("DELETE FROM link")
	c.db.Exec("DELETE FROM slowlog")
}

func (c Context) WriteEvent(e *parser.Event) {
	var currentFile string
	if eol := strings.IndexByte(e.Trace, '\n'); eol > 0 {
		currentFile = e.Trace[0:eol]
	}
	var currentFunction string
	if eol := strings.IndexByte(e.Trace, ' '); eol > 0 {
		currentFunction = e.Trace[0:eol]
	}
	_, err := c.insertEvent.Exec(
		e.Logname,
		e.Line,
		e.Time,
		e.Pool,
		e.Pid,
		e.Phpscript,
		e.Trace,
		currentFile,
		currentFunction,
	)
	if err != nil {
		panic(err)
	}
}

func (c Context) PrintEventsSummary() {
	c.db.Exec(`PRAGMA wal_checkpoint`)
	c.db.Exec(`CREATE INDEX slowlog_time ON slowlog("time")`)

	writer := csv.NewWriter(os.Stdout)
	writer.Comma = ';'
	defer writer.Flush()

	sql := `SELECT count(*), current_func FROM slowlog GROUP BY current_func ORDER BY count(*)`
	rows, err := c.db.Query(sql)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var row = make([]string, 2)
	for rows.Next() {
		if err := rows.Scan(&row[0], &row[1]); err != nil {
			panic(err)
		}
		writer.Write(row)
	}
}
