package sqlite

import (
	"database/sql"

	// required for sqlite
	_ "modernc.org/sqlite"
)

// Context wraps the sqlite connection
type Context struct {
	db     *sql.DB
	access struct {
		tx     *sql.Tx
		insert *sql.Stmt
		count  int
	}
	insertEvent *sql.Stmt
}

// InitDB returns a new connection to a sqlite file (created if necessary)
func InitDB(path string) Context {
	db, err := sql.Open("sqlite", path)
	if err != nil {
		panic(err)
	}

	// faster, but decreases safety
	db.Exec("PRAGMA synchronous = OFF")
	db.Exec("PRAGMA journal_mode = MEMORY")

	db.Exec(`CREATE TABLE IF NOT EXISTS slowlog
(logname text, line int, time int, pool text, pid int, phpscript text, trace text, current_file text, current_func text)`)
	db.Exec(`CREATE TABLE IF NOT EXISTS access
(ip text, time int, method text, uri text, querystring text, protocol text, status int, size int, referrer text, useragent text, filename text)`)
	db.Exec(`CREATE TABLE IF NOT EXISTS link
(slowlog_id int unique, access_id int, func text)`)

	insertEvent, err := db.Prepare("INSERT INTO slowlog VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}

	return Context{
		db:          db,
		insertEvent: insertEvent,
	}
}

// Close the connection
func (c Context) Close() {
	c.insertEvent.Close()
	c.db.Exec(`PRAGMA wal_checkpoint`)
	c.db.Close()
}
