package sqlite

import (
	"fmt"
)

func (c Context) ResetAnalysis() {
	c.db.Exec("DELETE FROM link")
}

type Event struct {
	Rowid    int
	Time     int
	Path     string
	funcname string
}

func (c Context) GetAccessInterval() (int, int) {
	var min, max int
	c.db.QueryRow("SELECT MIN(time), MAX(time) FROM access").Scan(&min, &max)
	return min, max
}

func (c Context) FindEvents(uri string, after int, before int, limit int) []Event {
	sql := fmt.Sprintf("SELECT rowid, time, phpscript FROM slowlog WHERE time > ? AND time < ? AND current_func = ? ORDER BY time DESC LIMIT %d", limit)
	rows, err := c.db.Query(sql, after, before, uri)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	events := make([]Event, 0)
	var event Event
	for rows.Next() {
		if err := rows.Scan(&event.Rowid, &event.Time, &event.Path); err != nil {
			panic(err)
		}
		events = append(events, event)
	}
	return events
}

func (c Context) FindAccess(e *Event, uri string, delay int, funcname string) {
	result, err := c.db.Exec(
		`INSERT OR REPLACE INTO link SELECT ?, MAX(rowid), ? FROM access WHERE time > ? AND time < ? AND uri = ? GROUP BY uri HAVING count(*) = 1`,
		e.Rowid,
		funcname,
		e.Time-delay-3,
		e.Time-delay+3,
		uri,
	)
	if err != nil {
		panic(err)
	}
	count, _ := result.RowsAffected()
	if count > 0 {
		fmt.Printf("Found a match in access log.\n")
	}
}

type minAccess struct {
	id        int
	ip        string
	uri       string
	time      int
	useragent string
}

func (c Context) PrintHistories(funcname string) {
	rows, err := c.db.Query("SELECT a.rowid, a.ip, a.uri, a.time, a.useragent FROM link JOIN access a ON a.rowid = link.access_id WHERE func = ?", funcname)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var row minAccess
	for rows.Next() {
		if err := rows.Scan(&row.id, &row.ip, &row.uri, &row.time, &row.useragent); err != nil {
			panic(err)
		}
		fmt.Printf("\nHistory for %s (%s)\n", row.uri, row.ip)
		c.printHistory(row)
	}
}

func (c Context) printHistory(a minAccess) {
	rows, err := c.db.Query(
		"SELECT a.time, a.method, a.uri, a.status, a.querystring FROM access a WHERE a.time > ? AND a.time <= ? AND ip = ? AND useragent = ? ORDER BY a.time",
		a.time-61, // 1 minute of history
		a.time,
		a.ip,
		a.useragent,
	)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var status, time int
	var method, uri, params string
	for rows.Next() {
		if err := rows.Scan(&time, &method, &uri, &status, &params); err != nil {
			panic(err)
		}
		fmt.Printf("%3d (%d) %4s %s%s\n", a.time-time, status, method, uri, params)
	}
}
