package sqlite

import (
	"github.com/lmorg/apachelogs"
)

func (c Context) ResetAccess() {
	c.db.Exec("DELETE FROM link")
	c.db.Exec("DELETE FROM access")
}

func (c *Context) StartAccessBatch() {
	var err error
	c.access.count = 0
	c.access.tx, err = c.db.Begin()
	if err != nil {
		panic(err)
	}

	c.access.insert, err = c.access.tx.Prepare("INSERT INTO access VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}
	if c.access.insert == nil {
		panic("empty prepared query")
	}
}

func (c *Context) EndAccessBatch() {
	if c.access.insert != nil {
		c.access.insert.Close()
	}
	c.access.tx.Commit()
}

func (c *Context) WriteAccess(r *apachelogs.AccessLine) {
	if c.access.insert == nil {
		var err error
		c.access.insert, err = c.db.Prepare("INSERT INTO access VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
		if err != nil {
			panic(err)
		}
	}
	if c.access.tx != nil {
		c.access.count++
		if c.access.count > 10000 {
			c.EndAccessBatch()
			c.StartAccessBatch()
		}
	}
	_, err := c.access.insert.Exec(
		r.IP,
		r.DateTime.Unix(),
		r.Method,
		r.URI,
		r.QueryString,
		r.Protocol,
		r.Status.I,
		r.Size,
		r.Referrer,
		r.UserAgent,
		r.FileName,
	)
	if err != nil {
		panic(err)
	}
}
