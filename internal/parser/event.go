package parser

// Event has all the info about a parsed event
type Event struct {
	Logname   string
	Line      int
	Time      int64
	Pool      string
	Pid       int
	Phpscript string
	Trace     string
}
