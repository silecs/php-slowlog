package parser

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"log"
	"regexp"
	"time"
)

// Parser holds the state while parsing the source
type Parser struct {
	scanner *bufio.Scanner
	row     int
	logname string
	reTitle *regexp.Regexp
}

// NewParser creates a parser for this source
func NewParser(reader io.Reader, logname string) *Parser {
	scanner := bufio.NewScanner(reader)
	return &Parser{
		scanner,
		0,
		logname,
		// e.g.: [11-Oct-2020 12:57:30]  [pool www] pid 29697
		regexp.MustCompile(`^\[(\d+-\w+-\d+ \d\d:\d\d:\d\d)\]\s+\[pool (.+?)\]\s+pid (\d+)`),
	}
}

// NextEvent returns the next event or nil if none was found
func (p *Parser) NextEvent() *Event {
	for p.scan() {
		line := p.scanner.Bytes()
		if len(line) == 0 || line[0] == ' ' {
			continue
		}
		if line[0] == '[' {
			return p.readEvent(line)
		}
	}
	return nil
}

func (p *Parser) scan() bool {
	p.row++
	return p.scanner.Scan()
}

func (p *Parser) readEvent(firstLine []byte) *Event {
	titleGroups := p.reTitle.FindSubmatch(firstLine)
	if titleGroups == nil {
		log.Fatalf("Error, title line did no match regexp: %+v\n", titleGroups)
	}
	// parse time with an implicit local time zone, like PHP seems to do
	time, err := time.Parse("02-Jan-2006 15:04:05", string(titleGroups[1]))
	if err != nil {
		log.Fatalf("Error, time was not parsed: %v\n", err)
	}
	event := Event{
		Logname: p.logname,
		Line:    p.row,
		Time:    time.Unix(),
		Pool:    string(titleGroups[2]),
	}
	n, err := fmt.Sscanf(string(titleGroups[3]), "%d", &event.Pid)
	if err != nil || n != 1 {
		log.Fatalf("Error, pid was not numeric (%d integers found) : %s / %v\n", n, string(titleGroups[3]), err)
	}

	p.scan()
	scriptLine := p.scanner.Bytes()
	if !bytes.Equal([]byte(`script_filename = `), scriptLine[0:18]) {
		log.Fatalf("Erreur : '%s'\n", string(scriptLine[0:18]))
	}
	event.Phpscript = string(scriptLine[18:])

	for p.scan() {
		line := p.scanner.Bytes()
		if len(line) == 0 || line[0] == ' ' {
			return &event
		}
		if line[0] != '[' {
			log.Fatalf("Error, found unexpected content in the trace: %v\n", string(line))
		}
		if p := bytes.IndexByte(line, ' '); p > 1 {
			event.Trace = event.Trace + string(line[(p+1):]) + "\n"
		} else {
			event.Trace = event.Trace + string(line) + "\n"
		}
	}
	return &event
}
