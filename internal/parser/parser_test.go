package parser

import (
	"bytes"
	"reflect"
	"testing"
)

func TestParser_NextEvent(t *testing.T) {
	tests := []struct {
		name string
		data []byte
		want *Event
	}{
		{
			name: "Single event",
			data: []byte(`[11-Oct-2020 12:57:30]  [pool www] pid 29697
script_filename = /srv/www/moodle-src/mod/quiz/processattempt.php
[0x00007fd1fda1d960] curl_exec() /srv/www/moodle-src/question/type/vplquestion/locallib.php:106
[0x00007fd1fda1d780] evaluate() /srv/www/moodle-src/question/type/vplquestion/question.php:91
`),
			want: &Event{
				Logname:   "X",
				Line:      1,
				Time:      1602421050,
				Pool:      "www",
				Pid:       29697,
				Phpscript: "/srv/www/moodle-src/mod/quiz/processattempt.php",
				Trace: `curl_exec() /srv/www/moodle-src/question/type/vplquestion/locallib.php:106
evaluate() /srv/www/moodle-src/question/type/vplquestion/question.php:91
`,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewParser(bytes.NewReader(tt.data), "X")
			if got := p.NextEvent(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Parser.NextEvent()\nGOT:  %+v\nWANT: %+v", got, tt.want)
			}
		})
	}
}
