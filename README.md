php-slowlog
===========

Analyze the log of slow requests in PHP-FPM.

Typical usage
-------------

### 1. Load the PHP slow log

The log is parsed and stored into a sqlite table.
It also displays the occurrences of slow PHP functions
(this is an approximation since the interruption for logging
can occur after the slow code was executed).
```bash
$ php-slowlog phpslow /var/log/php*-fpm-slow.log*
3;compute_filediff()
3;readfile()
12;curl_exec()
19;session_start()
```
So the slowlog contains 19 cases where `session_start()`
was interrupted because the script duration was above the threshold.

### 2. Load the access log

Load Apache's access log into a sqlite table.
It should be the same with Nginx and any web server following the Combined Log Format.
```bash
$ php-slowlog access /var/log/apache2/access.log*
# this is very slow (300k/min)
```

### 3. Display the slow navigation histories

Display the navigation histories that lead to a slow log
launching while the PHP function `session_start()` is called.
```bash
$ php-slowlog analyze --wwwroot='/srv/www' --duration=10 'session_start()'
History for /course/view.php (192.168.0.1)
 37 (200)  GET /course/view.php?id=56&sectionid=412
 36 (304)  GET /pluginfile.php/1/block_completion_levels/preset/0/1
 36 (304)  GET /pluginfile.php/29783/block_completion_levels/levels_pix/0/0
 34 (200) POST /lib/ajax/service.php?sesskey=AaLW5JcfR0&info=core_fetch_notifications
 31 (200) POST /lib/ajax/service.php?sesskey=AaLW5JcfR0&info=core_course_edit_module
  0 (200)  GET /course/view.php?id=56&sectionid=412
```

Configuration
-------------

Use `phpslow help [command]` to discover parameters.

Command options will read their default value from the file `~/.phpslow.toml`, if present.
```toml
# ~/.phpslow.toml

# The following parameters are required
# to identify the matching request in the access logs.

# Apache's DocumentRoot (Nginx's root)
# Used to deduce the request path from php's log.
wwwroot = "/srv/www"

# The value of PHP-FPM's request_slowlog_timeout.
# Used to guess when the initial request was received.
duration = 10
```
